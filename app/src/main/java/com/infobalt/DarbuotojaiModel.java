package com.infobalt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

@ManagedBean(name = "darbuotojaiModel")
@Named
@SessionScoped
public class DarbuotojaiModel implements Serializable {

	private List<Darbuotojas> darbuotojai = new ArrayList<Darbuotojas>();
	private Darbuotojas redaguojamasDarbuotojas = null;

	@PostConstruct
	public void postConstruct() {
		darbuotojai.add(new Darbuotojas(1, "Petras", "Pavardenis"));
		darbuotojai.add(new Darbuotojas(2, "Jonas", "Pavardenis"));
		darbuotojai.add(new Darbuotojas(3, "Onute", "Pavarde"));

	}

	public Darbuotojas searchDarbuotojasById(Integer id) {

		for (Darbuotojas darbuotojas : darbuotojai) {
			if (darbuotojas.getId() == id) {
				return darbuotojas;
			}
		}
		return null;
	}

	public List<Darbuotojas> getDarbuotojai() {
		return darbuotojai;
	}

	public void setDarbuotojai(List<Darbuotojas> darbuotojai) {
		this.darbuotojai = darbuotojai;
	}

	public Darbuotojas getRedaguojamasDarbuotojas() {
		return redaguojamasDarbuotojas;
	}

	public void setRedaguojamasDarbuotojas(Darbuotojas redaguojamasDarbuotojas) {
		this.redaguojamasDarbuotojas = redaguojamasDarbuotojas;
	}

}
