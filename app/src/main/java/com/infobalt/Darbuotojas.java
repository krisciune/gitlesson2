package com.infobalt;

public class Darbuotojas {
	private Integer id;
	private String vardas;
	private String pavarde;
	
	public Darbuotojas() {
		id = 0;
		vardas = "";
		pavarde = "";
	}
	
	public Darbuotojas(int id, String vardas, String pavarde) {
		this.id = id;
		this.vardas = vardas;
		this.pavarde = pavarde;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVardas() {
		return vardas;
	}
	public void setVardas(String vardas) {
		this.vardas = vardas;
	}
	public String getPavarde() {
		return pavarde;
	}
	public void setPavarde(String pavarde) {
		this.pavarde = pavarde;
	}
	
}
